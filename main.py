""" This script updates data in db by matching values from PSX"""
import sys
import os
import datetime
from cassandra.cluster import Cluster

def compare_value(PSX, DB):
    """Compares all the values if values does not correspond function returns false."""
    PSX = float(PSX)
    if PSX == DB:
        return True
    return False

def parse_data(date):
    """ Downloads file from market_summary and un compresses it"""
    os.system("curl -O https://dps.psx.com.pk/dataportal/download/mkt_summary/"+date+".Z")
    os.system("uncompress *.Z")

    with open(date, "r")as f_p:
        content = f_p.readlines()

    cluster = Cluster()
    session = cluster.connect('market')

    t_a = date + ' 00:00:00'
    t_b = date + ' 23:59:00'

    for line in content:
        split_line = line.split('|')
        symbol = split_line[1]
        open_ = split_line[4]
        high = split_line[5]
        low = split_line[6]
        close = split_line[7]
        volume = split_line[8]
        ldcp = split_line[9]

        ta_date_time = datetime.datetime.strptime(t_a, '%Y-%m-%d %H:%M:%S')
        tb_date_time = datetime.datetime.strptime(t_b, '%Y-%m-%d %H:%M:%S')

        stmt = "SELECT * FROM market.daily WHERE symbol=? AND time>? AND time <?"
        output = session.execute(session.prepare(stmt), [symbol, ta_date_time, tb_date_time])

        if output.current_rows:
            current_row = output.current_rows[0]
            report_row = {
                'close': compare_value(close, current_row[2]),
                'high': compare_value(high, current_row[5]),
                'ldcp': compare_value(ldcp, current_row[6]),
                'low': compare_value(low, current_row[7]),
                'open': compare_value(open_, current_row[8]),
                'volume': compare_value(volume, current_row[11]),
            }

            keys = report_row.keys()
            db_date = current_row[1]

            for key in keys:
                if not report_row[key]:
                    params = (float(high), float(ldcp), float(low),
                              float(open_), int(volume), db_date, symbol)
                    session.execute("UPDATE market.daily SET high = %s,"
                                    "ldcp = %s,low = %s,open=%s,volume = %s"
                                    "where time = %s AND symbol = %s ", params)
                    print("row updated")
                    break
                else:
                    print("no update required")
                    continue


def run(date):
    """parse data compares PSX values and updates DB"""
    parse_data(date[0])



if __name__ == '__main__':
    run(sys.argv[1:])
